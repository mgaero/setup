#!/usr/bin/env bash

# install oh-my-zsh
# ------------------------------------------------------------------------------
dconf load /org/gnome/terminal/ < "$(pwd)"/resources/gnome-terminal-profiles.dconf
usermod --shell "$(which zsh)" "$USER"
sh ./resources/oh-my-zsh-install.sh

cp ./resources/.zshrc "$HOME"
source "$HOME"/.zshrc

cd ./resources/fonts
./install.sh
cd ../..

# dconf dump /org/gnome/terminal/legacy/profiles:/ > ~/Desktop/gnome-terminal-profiles.dconf
