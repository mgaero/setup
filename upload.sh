#!/usr/bin/bash

source ./bin/shared.sh
# | grep -E '^\s*(deleting|new|updated)'
for host in "${HOSTNAMES[@]}"; do
    echo "Uploading setup to $host:~/.aero"
    rsync -azv --exclude='*.tmp'  --exclude='*.deb' --exclude='*.tar.gz' --exclude='*.exe' --exclude='.git' --exclude='.idea' . "$host":~/.aero/
#    ssh "$host" bash -l -c '"rm -fr ~/.bin"'
#    ssh "$host" bash -l -c '"sudo rm -f /etc/cron.d/admin_cron"'
#    ssh "$host" bash -l -c '"rm ~/check_wg.log ~/restart_autossh.log"'
    ssh "$host" bash -l -c '"mkdir -p ~/logs"'
#    ssh "$host" bash -l -c '"sudo cp ~/.aero/resources/admin_cron /etc/cron.d/admin_cron"'
    echo -e "Done.\n\n"
done

