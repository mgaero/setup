#!/bin/bash

print() {
  local message="$1"
  echo "------------------------------------------------------------------------------"
  echo "$message"
  echo "------------------------------------------------------------------------------"
}

current_uid=$(id -u)
# Check if the current user is root
if [ "$current_uid" -ne 0 ]; then
    echo "run as root."
    exit
fi

# Check if winehq-archive.key exists
file_name="/etc/apt/keyrings/winehq-archive.key"
if [ -f "$file_name" ]; then
    echo "OK - wine key exists."
else
    echo "NOK OK - wine key does not exist."
fi

# Check if sources file exists
code_name=$(lsb_release -c --short)
file_name="/etc/apt/sources.list.d/winehq-$code_name.sources"
if [ -f "$file_name" ]; then
    echo "OK - wine sources file exists."
else
    echo "NOT OK wine sources file does not exist."
fi

# Check if WineHQ packages are installed
if dpkg -l | grep -q winehq-stable; then
    echo "OK - WineHQ packages are installed."
else
    echo "NOK OK - WineHQ packages are not installed."
fi


# Check if aeronet user is in sudo and dialout groups
if groups "aeronet" | grep -q "\bsudo\b" && groups "aeronet" | grep -q "\bdialout\b"; then
    echo "OK - aeronet user is in sudo and dialout groups."
else
    echo "NOK OK -aeronet user is not in sudo and dialout groups."
fi

# Check if aeronet user's shell is set to zsh
if getent passwd "aeronet" | cut -d: -f7 | grep -q "zsh"; then
    echo "OK - aeronet user's shell is set to zsh."
else
    echo "NOK OK - aeronet user's shell is not set to zsh."
fi

# Check if authorized_keys file contains correct public keys
file_path="/home/aeronet/.ssh/authorized_keys"
public_key=$(cat ./resources/aeronet.pub)
public_key2=$(cat ./resources/aeronetnew.pub)
if grep -q "$public_key" "$file_path" && grep -q "$public_key2" "$file_path"; then
    echo "OK - Authorized keys are correct."
else
    echo "NOK OK - Authorized keys are not correct."
fi

# Check if admaeronetin user has sudo privileges without password
if sudo grep -q '^aeronet ALL=(ALL) NOPASSWD:ALL' /etc/sudoers; then
    echo "OK - aeronet user has sudo privileges without password."
else
    echo "NOK OK - aeronet user does not have sudo privileges without password."
fi

# Check if sshd_config contains correct configuration
file_path="/etc/ssh/sshd_config"
line1="Match User aeronet"
line2="PasswordAuthentication no"
line3='AuthenticationMethods "publickey"'
if grep -Fxq "$line1" "$file_path" && grep -Fxq "$line2" "$file_path" && grep -Fxq "$line3" "$file_path"; then
    echo "OK - sshd_config contains correct configuration."
else
    echo "NOK OK - sshd_config does not contain correct configuration."
fi


# List of required packages
required_packages=(
    "winehq-stable"
    "net-tools"
    "speedtest"
    "openssh-server"
    "zsh"
    "picocom"
    "autossh"
    "make"
    "rtklib"
    "curl"
    "git"
    "wget"
    "telnet"
    "htop"
    "wireguard"
    "telegraf"
)

# Check if each required package is installed
missing_packages=()
for package in "${required_packages[@]}"; do
    if dpkg -l | grep -q "^ii  $package"; then
        echo "OK - $package is installed."
    else
        echo "NOT OK - $package is not installed."
        missing_packages+=("$package")
    fi
done

# Print missing packages, if any
if [ ${#missing_packages[@]} -eq 0 ]; then
    echo "OK - All required packages are installed."
else
    echo "NOT OK - The following packages are missing:"
    printf '%s\n' "${missing_packages[@]}"
fi


# ------------------------------------------------------------------------------
if [ -d "/usr/local/go" ]; then
    echo "OK - Go is installed."
else
    echo "NOK OK - Go is not installed."
fi

# ------------------------------------------------------------------------------
package="nomachine"
if dpkg -s "$package" 2>/dev/null | grep -q 'Status: install ok installed'; then
    echo "OK - Nomachine is installed."
else
    echo "NOK OK - Nomachine is not installed."
fi

# package="realvnc-vnc-server"
# if dpkg -s "$package" 2>/dev/null | grep -q 'Status: install ok installed'; then
#     echo "OK - RealVNC Server is installed."
# else
#     echo "NOK OK - RealVNC Server is not installed."
# fi




print "Verification completed."
