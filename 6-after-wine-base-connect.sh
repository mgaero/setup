#!/usr/bin/env bash

echo "changing to $HOME/.wine/dosdevices"
cd $HOME/.wine/dosdevices || exit
ln -s /dev/ttyUSB_UBX1 com41
ln -s /dev/ttyUSB_UBX2 com42

echo "dev tty"
ls -l /dev/ttyUSB_UBX*

echo "wine tty"
ls -l $HOME/.wine/dosdevices | grep ttyUSB_UBX