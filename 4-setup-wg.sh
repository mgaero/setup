#!/usr/bin/env bash

current_uid=$(id -u)
# Check if the current user is root
if [ "$current_uid" -ne 0 ]; then
    echo "run as root."
    exit
fi

print() {
  local message="$1"
  echo -e "$message"
  echo "------------------------------------------------------------------------------"
}

# Define an associative array of hostname and wg ip
# ------------------------------------------------------------------------------
declare -A hosts
hosts["ubuntu"]="172.20.30.1"
hosts["TampaShore1"]="172.20.18.1"
hosts["TampaShore2"]="172.20.18.2"
hosts["KeyWestShore1"]="172.20.20.1"
hosts["KeyWestShore2"]="172.20.20.2"
hosts["CozumelShore1"]="172.20.22.1"
hosts["CozumelShore2"]="172.20.22.2"
hosts["ProgressoShore1"]="172.20.24.1"
hosts["ProgressoShore2"]="172.20.24.2"
hosts["IslanderBow1"]="172.20.17.3"
hosts["IslanderBow2"]="172.20.17.4"
hosts["IslanderStern1"]="172.20.17.5"
hosts["IslanderStern2"]="172.20.17.6"

# Get the value based on the hostname
hostname=$(hostname)
wg_ip=${hosts[$hostname]}

# Check if the hostname exists in the array
if [ -z "$wg_ip" ]; then
    echo "Hostname '$hostname' not found in the array. Exiting."
    exit 1
fi
# Print the value

echo -e "\n\n"
echo "Value for $hostname is: $wg_ip"


# enable IPv4 forwarding
# ------------------------------------------------------------------------------
sysctl -w net.ipv4.ip_forward=1 &>/dev/null
perl -pi -e 's/#{1,}?net.ipv4.ip_forward ?= ?(0|1)/net.ipv4.ip_forward = 1/g' /etc/sysctl.conf

# check active network interface name
net_interface=$(ip link | awk -F: '$0 !~ "wg|lo|vir|wl|enx|^[^0-9]"{print $2;getline}' |  sed -e 's/^[[:space:]]*//')
echo "active interface: $net_interface"

# create private/public key
if [ ! -f /etc/wireguard/privatekey ]; then
    cd /etc/wireguard && wg genkey | tee privatekey | wg pubkey > publickey
fi

private_key=$(cat /etc/wireguard/privatekey)

# create wg0.conf
# ------------------------------------------------------------------------------
# these 2 lines are specific to nucbox g2 devices


hostname=$(hostname)
case "$hostname" in
    # shore mappings
    TampaShore1|TampaShore2|KeyWestShore1|KeyWestShore2|CozumelShore1|CozumelShore2|ProgressoShore1|ProgressoShore2|IslanderBow1|IslanderBow2|IslanderStern1|IslanderStern2)
        add_postrouting="iptables -t nat -A POSTROUTING -o enp1s0 -j MASQUERADE; iptables -t nat -A POSTROUTING -o enp3s0 -j MASQUERADE; iptables -t nat -A POSTROUTING -o wlp2s0 -j MASQUERADE;"
        del_postrouting="iptables -t nat -D POSTROUTING -o enp1s0 -j MASQUERADE; iptables -t nat -D POSTROUTING -o enp3s0 -j MASQUERADE; iptables -t nat -D POSTROUTING -o wlp2s0 -j MASQUERADE;"
        ;;

    # islander mappings
    *)
        add_postrouting="iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE;"
        del_postrouting="iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE;"
        ;;
esac

echo "--------------------------------------------------------------------------"
echo "[Interface]
Address = $wg_ip/32
PostUp = iptables -A FORWARD -i wg0 -j ACCEPT; iptables -A FORWARD -o wg0 -j ACCEPT; $add_postrouting
PostDown = iptables -D FORWARD -i wg0 -j ACCEPT; iptables -D FORWARD -o wg0 -j ACCEPT; $del_postrouting
PrivateKey = $private_key

[Peer]
PublicKey = 0uw3p1ywAEswBzaz+4qdcW8/ij31+CcTrYMzZ2e9fVo=
AllowedIPs = 172.20.0.0/16
Endpoint = 95.217.117.84:51820
PersistentKeepalive = 25" > /etc/wireguard/wg0.conf
echo "--------------------------------------------------------------------------"

# check wg0.conf
# ------------------------------------------------------------------------------

echo -e "\n\n"
print "wg0.conf"
cat /etc/wireguard/wg0.conf

# hetzner wg configuration entry
# ------------------------------------------------------------------------------

echo -e "\n\n\n\n"
echo "--------------------------------------------------------------------------"
print "Hetzner WG configuration entry"

echo "[Peer]
# $hostname
PublicKey = $(cat /etc/wireguard/publickey)
AllowedIPs = $wg_ip/32
PersistentKeepalive = 25"
echo -e "\n--------------------------------------------------------------------------"

# enable and restart wg
# ------------------------------------------------------------------------------
echo -e "\n\n"
systemctl restart wg-quick@wg0
systemctl enable wg-quick@wg0
# wg-quick down wg0 &>/dev/null
# wg-quick up wg0 &>/dev/null
wg


print "done."