#!/usr/bin/bash

source ./shared.sh

# Loop over the hostnames array and upload files with scp
for host in "${HOSTNAMES[@]}"; do
    echo "Uploading admnin_cron to $host..."
    scp "$(pwd)/.../resources/admnin_cron" "$host":/etc/cron.d/
done