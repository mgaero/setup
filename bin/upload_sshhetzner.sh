#!/usr/bin/bash

source ./shared.sh

# Loop over the hostnames array and upload files with scp
for host in "${HOSTNAMES[@]}"; do
    echo "Connecting to $host"
    ssh "$host" bash -l -c "'sudo apt update && sudo apt install autossh; cd ~/.aero/bin; ./execute_sshhetzner.sh'"
done



# sudo nano /etc/systemd/system/sshhetzner.service
# sudo systemctl daemon-reload
