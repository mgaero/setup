#!/usr/bin/bash

source ./shared.sh

# Loop over the hostnames array and upload files with scp
for host in "${HOSTNAMES[@]}"; do
    echo "Uploading .zshrc to $host..."
    scp "$(pwd)/../resources/.zshrc" "$host":~
done