#!/usr/bin/bash

source ./shared.sh

echo "running script"

# Loop over the hostnames array and upload files with scp
for host in "${HOSTNAMES[@]}"; do
    echo "Uploading .zprofile to $host..."
    scp "$(pwd)/../resources/.zprofile" "$host":~
done