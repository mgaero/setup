#!/usr/bin/env bash

source ./shared.sh

# Get the hostname (or set it manually)
HOSTNAME=$(hostname)

# Get the port from the map
REMOTE_PORT=${REMOTE_PORTS[$HOSTNAME]}
echo -e "Hostname: $HOSTNAME\tPORT: $REMOTE_PORT"
# Check if the port exists for this hostname
if [ -z "$REMOTE_PORT" ]; then
    echo "Error: No remote port found for hostname $HOSTNAME"
    exit 1
fi

TEMPLATE_FILE="$HOME/.aero/resources/sshhetzner.service"
SERVICE_FILE="/etc/systemd/system/sshhetzner.service"
#SERVICE_FILE="/tmp/sshhetzner.service"

# Replace [[REMOTE_PORT]] with the actual port and write the updated service file
sed "s/\[\[REMOTE_PORT\]\]/$REMOTE_PORT/g" "$TEMPLATE_FILE" | sed "s/\[\[USER\]\]/$(whoami)/g" | sudo tee "$SERVICE_FILE" > /dev/null

# Reload the systemd daemon and restart the service
sudo systemctl daemon-reload
sudo systemctl restart sshhetzner.service
echo "Done."