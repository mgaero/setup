#!/usr/bin/env bash

current_uid=$(id -u)
# Check if the current user is root
if [ "$current_uid" -ne 0 ]; then
  echo "run as root."
  exit
fi

# array of strings
commands=(
"aeronet ALL=(ALL) NOPASSWD:/bin/systemctl status platform"
"aeronet ALL=(ALL) NOPASSWD:/bin/systemctl restart platform"
"aeronet ALL=(ALL) NOPASSWD:/bin/systemctl enable platform"
"aeronet ALL=(ALL) NOPASSWD:/bin/systemctl disable platform"
"aeronet ALL=(ALL) NOPASSWD:/bin/systemctl stop platform"

"aeronet ALL=(ALL) NOPASSWD:/bin/systemctl status controller"
"aeronet ALL=(ALL) NOPASSWD:/bin/systemctl restart controller"
"aeronet ALL=(ALL) NOPASSWD:/bin/systemctl enable controller"
"aeronet ALL=(ALL) NOPASSWD:/bin/systemctl disable controller"
"aeronet ALL=(ALL) NOPASSWD:/bin/systemctl stop controller"

"aeronet ALL=(ALL) NOPASSWD:/bin/systemctl status node_actions"
"aeronet ALL=(ALL) NOPASSWD:/bin/systemctl restart node_actions"
"aeronet ALL=(ALL) NOPASSWD:/bin/systemctl enable node_actions"
"aeronet ALL=(ALL) NOPASSWD:/bin/systemctl disable node_actions"
"aeronet ALL=(ALL) NOPASSWD:/bin/systemctl stop node_actions"

"aeronet ALL=(ALL) NOPASSWD:/bin/systemctl status untangle"
"aeronet ALL=(ALL) NOPASSWD:/bin/systemctl restart untangle"
"aeronet ALL=(ALL) NOPASSWD:/bin/systemctl enable untangle"
"aeronet ALL=(ALL) NOPASSWD:/bin/systemctl disable untangle"
"aeronet ALL=(ALL) NOPASSWD:/bin/systemctl stop untangle"
)

# Loop over each string in the array
for command in "${commands[@]}"; do
  # Check if the command exists in the /etc/sudoers file
  if ! grep -q "^$command" /etc/sudoers; then
    # Add the command to the /etc/sudoers file
    echo "$command" | tee -a /etc/sudoers > /dev/null
    echo "OK - Added $command to /etc/sudoers."
  else
    echo "PASS - already exists in /etc/sudoers [ $command ]"
  fi
done
