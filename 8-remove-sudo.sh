#!/usr/bin/env bash

current_uid=$(id -u)
# Check if the current user is root
if [ "$current_uid" -ne 0 ]; then
  echo "run as root."
  exit
fi

gpasswd --delete aeronet sudo &>/dev/null