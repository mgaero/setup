#!/usr/bin/bash

source ./bin/shared.sh

for host in "${HOSTNAMES[@]}"; do
    echo "Uploading .zprofile to $host..."
    rsync -avz --delete  --exclude='*.tmp'  --exclude='*.deb' --exclude='*.tar.gz' --exclude='*.exe' --exclude='.git' --exclude='.idea' . "$host":~/.setup/
done
