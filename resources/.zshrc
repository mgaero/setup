bindkey -e
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zsh_history
setopt    appendhistory     #Append history to the history file (no overwriting)
setopt    sharehistory      #Share history across terminals
setopt    incappendhistory  #Immediately append to the history file, not just when a term is killed

export ZSH="$HOME/.oh-my-zsh"
ZSH_THEME="agnoster"
zstyle ':omz:update' mode auto      # update automatically without asking
plugins=(git)

source $ZSH/oh-my-zsh.sh

export PATH=$PATH:/usr/local/go/bin:$HOME/go/src/taperline/bin
export GOPRIVATE=*gitlab.com/aeronetglobal

cd $HOME/taperline/logs &>/dev/null

alias tf='tail -f'
alias ls='ls -lh'
alias ll='ls -lht'
alias la='ls -lhA'
alias logs='cd $HOME/taperline/logs'
alias tps='cd $HOME/go/src/taperline'
alias tp='cd $HOME/taperline'
alias tlp='tail -f $HOME/taperline/logs/platform.log'
alias tlc='tail -f $HOME/taperline/logs/controller.log'
alias nodec='cd $HOME/node_actions'
alias lsdev='ls -l /dev | egrep -i "usb|acm"'


# sudo nano /etc/cron.d/admin_cron


# * * * * * admin . /home/admin/.zprofile /home/admin/go/src/taperline/bin/restart_autossh.sh >> /home/admin/restart_autossh.log 2>&1
# * * * * * admin . /home/admin/.zprofile /home/admin/go/src/taperline/bin/check_wg.sh >> /home/admin/check_wg.log 2>&1