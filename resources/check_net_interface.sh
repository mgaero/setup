#!/bin/bash

ip link | awk -F: '$0 !~ "wg|lo|vir|wl|enx|^[^0-9]"{print $2;getline}' |  sed -e 's/^[[:space:]]*//'