#!/usr/bin/env bash

current_uid=$(id -u)
# Check if the current user is root
if [ "$current_uid" -ne 0 ]; then
  echo "run as root."
  exit
fi

# root password
echo root:T4EuzF4M7DjQngIAr4mBsC289mEaSRAN | chpasswd

print() {
  local message="$1"
  echo "$message"
  echo "------------------------------------------------------------------------------"
}

check_exit_status() {
  local exit_status=$1

  if [ "$exit_status" -ne 0 ]; then
    echo "Command failed with exit status $exit_status. Exiting."
    exit "$exit_status"
  fi
}

code_name=$(lsb_release -c --short)
# ------------------------------------------------------------------------------
# wine
print "Checking wineqh key..."
file_name="/etc/apt/keyrings/winehq-archive.key"
if ! [ -e "$file_name" ]; then
  wget -O "$file_name" https://dl.winehq.org/wine-builds/winehq.key &>/dev/null
fi

print "Checking sources file..."
file_name="winehq-$code_name.sources"
if ! [ -e "/etc/apt/sources.list.d/$file_name" ]; then
  wget -NP /etc/apt/sources.list.d/ "https://dl.winehq.org/wine-builds/ubuntu/dists/$code_name/$file_name" &>/dev/null
fi

# ------------------------------------------------------------------------------
print "Installing packages..."

apt -qq update &&
  dpkg --add-architecture i386 &&
  mkdir -pm755 /etc/apt/keyrings &&
  apt -qq update &&
  bash ./resources/speedtest-script.deb.sh &>/dev/null &&
  apt -qq update &&
  apt -qq install --install-recommends winehq-stable net-tools speedtest openssh-server zsh picocom autossh make rtklib curl git wget vlc telnet htop wireguard telegraf -y
wine --version &&
  sudo systemctl enable --now ssh

## ------------------------------------------------------------------------------
## Check if admin user exists
#print "Checking admin user..."
#if ! grep -q "^admin:" /etc/passwd; then
#    useradd -m admin || exit
#fi

## ------------------------------------------------------------------------------
#print "add groups"
#usermod -aG sudo,dialout admin &>/dev/null
usermod --shell /bin/zsh aeronet &>/dev/null
usermod -aG dialout aeronet &>/dev/null
# ------------------------------------------------------------------------------
# create authorized keys file
print "authorized_keys"
#mkdir -p /home/admin/.ssh /home/aeronet/.ssh
#file_path="/home/admin/.ssh/authorized_keys"
#if [ ! -f "$file_path" ]; then
#    touch "$file_path"
#fi
file_path="/home/aeronet/.ssh/authorized_keys"
if [ ! -f "$file_path" ]; then
  touch "$file_path"
fi

public_key=$(cat ./resources/aeronet.pub)
public_key2=$(cat ./resources/aeronetnew.pub)

## add for admin
#cp ./resources/aeronet* /home/admin/.ssh
#cp ./resources/config /home/admin/.ssh
#cp ./resources/.gitconfig /home/admin
#echo -e "$public_key" > /home/admin/.ssh/authorized_keys
#echo -e "$public_key2" >> /home/admin/.ssh/authorized_keys
#chown -R admin:admin /home/admin
#chmod -R 700 /home/admin/.ssh

# add for aeronet
cp ./resources/aeronet* /home/aeronet/.ssh
cp ./resources/config /home/aeronet/.ssh
cp ./resources/.gitconfig /home/aeronet
echo -e "$public_key" >/home/aeronet/.ssh/authorized_keys
echo -e "$public_key2" >>/home/aeronet/.ssh/authorized_keys
sudo chown -R aeronet:aeronet /home/aeronet
sudo chmod -R 700 /home/aeronet/.ssh
# ------------------------------------------------------------------------------
# add to sudoers file
print "add to sudoers file"
#grep -qxF -- "aeronet ALL=(ALL) NOPASSWD:ALL" /etc/sudoers || echo "aeronet ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
./add-to-sudoers.sh

# ------------------------------------------------------------------------------
cp ./resources/controller.service /etc/systemd/system/controller.service
cp ./resources/platform.service /etc/systemd/system/platform.service
cp ./resources/node_actions.service /etc/systemd/system/node_actions.service
cp ./resources/untangle.service /etc/telegraf/untangle.service
cp ./resources/telegraf.conf /etc/telegraf/telegraf.conf

systemctl enable node_actions &>/dev/null
systemctl enable telegraf &>/dev/null && systemctl restart telegraf &>/dev/null

# ------------------------------------------------------------------------------
# update ssh config file
file_path=/etc/ssh/sshd_config
perl -pi -e 's/#PermitRootLogin prohibit-password/PermitRootLogin no/g' "$file_path"
perl -pi -e 's/PermitRootLogin prohibit-password/PermitRootLogin no/g' "$file_path"

line1="Match User aeronet"
line2="PasswordAuthentication no"
line3='AuthenticationMethods "publickey"'

# Check if all three lines exist in the file in that order
if ! grep -Fxq "$line1" "$file_path" || ! grep -Fxq "$line2" "$file_path" || ! grep -Fxq "$line3" "$file_path"; then
  # Append the lines to the file
  {
    echo "$line1"
    echo "$line2"
    echo "$line3"
  } >>"$file_path"
else
  echo "All lines already exist in the $file_path in the specified order."
fi
systemctl restart ssh &>/dev/null

# ------------------------------------------------------------------------------
# add cron job to ping server
#echo "* * * * * aeronet /bin/ping -i 15 172.20.0.1 -c 1 -w 30 > &>/dev/null" >/etc/cron.d/ping_cron

# ------------------------------------------------------------------------------
# install go
go_zip=./resources/go1.21.8.linux-amd64.tar.gz
if [ -d /usr/local/go ]; then
  rm -rf /usr/local/go
  check_exit_status $?
fi
tar -C /usr/local -xzf "$go_zip"
check_exit_status $?

# ------------------------------------------------------------------------------
# nomachine
print "Installing nomachine..."
package=nomachine
if dpkg -s $package 2>/dev/null | grep -q 'Status: install ok installed'; then
  echo "$package - status installed."
else
  echo "$package - status not installed."
  dpkg -i ./resources/nomachine_8.11.3_4_amd64.deb
  check_exit_status $?
fi

# ------------------------------------------------------------------------------
print "change password"
#echo "aeronet:aer0net//101" | chpasswd


# serial mappings
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

print "add serial mappings"
# ------------------------------------------------------------------------------
add_shore_mapping() {
  commands=(
    'SUBSYSTEM=="tty", ATTRS{idVendor}=="1546", ATTRS{idProduct}=="01a9", MODE="0660", SYMLINK+="ttyUSB_UBX1"'
    'SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6015", MODE="0660", ATTRS{serial}=="D30I19XM", MODE="0660", SYMLINK+="ttyUSB_UBX2"'
    'SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6015", MODE="0660", ATTRS{serial}=="D30HPUN3", MODE="0660", SYMLINK+="ttyUSB_UBX2"'
    'SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6015", MODE="0660", ATTRS{serial}=="D30HPSS0", MODE="0660", SYMLINK+="ttyUSB_UBX2"'
    'SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6015", MODE="0660", ATTRS{serial}=="D30I1AME", MODE="0660", SYMLINK+="ttyUSB_UBX2"'
  )

  filename="/etc/udev/rules.d/10-usb-serial.rules"
  for command in "${commands[@]}"; do
    if ! grep -q "^$command" "$filename"; then
      echo "$command" | tee -a "$filename" >/dev/null
      echo "OK - Added $command"
    fi
  done

  udevadm trigger
  ls -l /dev/ttyUSB_UBX*

}

add_islander_mapping() {
  commands=(
    'SUBSYSTEM=="tty", ATTRS{idVendor}=="152a", ATTRS{idProduct}=="85c0", ATTRS{serial}=="3682642", MODE="0660", SYMLINK+="ttyUSB_MX1"'
    'SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6015", ATTRS{serial}=="D30I3CRA", MODE="0660", SYMLINK+="ttyUSB_MX2"'
    'SUBSYSTEM=="tty", ATTRS{idVendor}=="152a", ATTRS{idProduct}=="85c0", ATTRS{serial}=="3692089", MODE="0660", SYMLINK+="ttyUSB_MX1"'
    'SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6015", ATTRS{serial}=="D30I38LU", MODE="0660", SYMLINK+="ttyUSB_MX2"'
  )

  filename="/etc/udev/rules.d/10-usb-serial.rules"
  for command in "${commands[@]}"; do
    if ! grep -q "^$command" "$filename"; then
      echo "$command" | tee -a "$filename" >/dev/null
      echo "OK - Added $command"
    fi
  done

  udevadm trigger
  ls -l /dev/ttyUSB_MX*
}

# Get the hostname
hostname=$(hostname)
case "$hostname" in
# shore mappings
TampaShore1 | TampaShore2 | KeyWestShore1 | KeyWestShore2 | CozumelShore1 | CozumelShore2 | ProgressoShore1 | ProgressoShore2)
  echo "Hostname '$hostname'. Running add_shore_mapping"
  add_shore_mapping
  ;;

# islander mappings
IslanderBow1 | IslanderBow2 | IslanderStern1 | IslanderStern2)
  echo "Hostname '$hostname'. Running add_islander_mapping.."
  add_islander_mapping
  ;;

esac

echo "done."


sudo nano /etc/cron.d/admin_cron
* * * * * admin /home/admin/go/src/taperline/bin/restart_autossh.sh >> /var/log/restart_autossh.log 2>&1
* * * * * admin /home/admin/go/src/taperline/bin/check_wg.sh >> /home/admin/check_wg.log 2>&1