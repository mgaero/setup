upload_setup:
	bash $(CURDIR)/upload.sh

copy_zprofile:
	cd  $(CURDIR)/bin && bash $(CURDIR)/bin/upload_zprofile.sh

copy_zshrc:
	cd  $(CURDIR)/bin && bash $(CURDIR)/bin/upload_zshrc.sh

auto_ssh:
	cd $(CURDIR)/bin && bash $(CURDIR)/bin/upload_sshhetzner.sh

