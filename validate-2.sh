#!/bin/bash

print() {
  local message="$1"
  echo "------------------------------------------------------------------------------"
  echo "$message"
  echo "------------------------------------------------------------------------------"
}

# Get the current username
current_user=$(whoami)
# Check if the current user is "aeronet"
if [ ! "$current_user" = "aeronet" ]; then
    echo "run as user 'aeronet'"
    exit
fi


# ------------------------------------------------------------------------------
directories=(
    "$HOME/go/src"
    "$HOME/taperline/logs"
    "$HOME/node_actions"
)
missing_directories=()
for dir in "${directories[@]}"; do
    if [ ! -d "$dir" ]; then
        missing_directories+=("$dir")
    fi
done
if [ ${#missing_directories[@]} -eq 0 ]; then
    echo "OK - All directories are present."
else
    echo "NOK OK - The following directories are missing:"
    printf '\t%s\n' "${missing_directories[@]}"
fi

# ------------------------------------------------------------------------------
keys=(
    "$HOME/.ssh/aeronet"
    "$HOME/.ssh/aeronetnew"
    "$HOME/.ssh/config"
)
missing_keys=()
for key in "${keys[@]}"; do
    if [ ! -f "$key" ]; then
        missing_keys+=("$key")
    fi
done
if [ ${#missing_keys[@]} -eq 0 ]; then
    echo "OK - All SSH keys are present."
else
    echo "NOK OK - The following SSH keys are missing:"
    printf '\t%s\n' "${missing_keys[@]}"
fi

# ------------------------------------------------------------------------------
repositories=(
    "$HOME/go/src/taperline"
    "$HOME/go/src/aeronet-node-service"
)
missing_repositories=()
for repo in "${repositories[@]}"; do
    if [ ! -d "$repo" ]; then
        missing_repositories+=("$repo")
    fi
done
if [ ${#missing_repositories[@]} -eq 0 ]; then
    echo "OK - All Git repositories are cloned."
else
    echo "NOK OK - The following Git repositories are missing:"
    printf '\t%s\n' "${missing_repositories[@]}"
fi

print "Validation completed."
