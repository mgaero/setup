#!/usr/bin/env bash

# Get the current username
current_user=$(whoami)
# Check if the current user is "aeronet"
if [ ! "$current_user" = "aeronet" ]; then
    echo "run as user 'aeronet'"
    exit
fi

check_exit_status() {
    local exit_status=$1

    if [ "$exit_status" -ne 0 ]; then
        echo "Command failed with exit status $exit_status. Exiting."
        exit "$exit_status"
    fi
}


print() {
  local message="$1"
  echo "$message"
  echo "------------------------------------------------------------------------------"
}


# ------------------------------------------------------------------------------
cp "./resources/ublox-base.txt" "$HOME"

# ------------------------------------------------------------------------------
print "add dirs"
mkdir -p "$HOME/go/src" "$HOME/taperline/logs" "$HOME/node_actions"
check_exit_status $?
# ------------------------------------------------------------------------------

sudo chown -R aeronet:aeronet "$HOME"
sudo chmod -R 700 "$HOME/taperline" "$HOME/node_actions"
sudo chmod -R 700 "$HOME/.ssh"


# Attempt to change directory
if cd ~/go/src; then
    echo "Successfully changed to ~/go/src"
else
    echo "Failed to change to ~/go/src"
    exit 1
fi

print "checking taperline"
if [ ! -d /home/aeronet/go/src/taperline ]; then
    git clone git@gitlab.com:aeronetglobal/taperline.git
    check_exit_status $?
fi
cd "$HOME/go/src/taperline" && git config pull.rebase false && git pull &>/dev/null
check_exit_status $?

print "checking aeronet-node-service"
cd ~/go/src || exit
if [ ! -d /home/aeronet/go/src/aeronet-node-service ]; then
    git clone git@gitlab.com:aeronetglobal/aeronet-node-service.git
    check_exit_status $?
fi
cd "$HOME/go/src/aeronet-node-service" && git config pull.rebase false && git pull &>/dev/null
check_exit_status $?
# ------------------------------------------------------------------------------

print "done."